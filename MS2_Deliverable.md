# Wiki
For this microsprint we chose to work on two topics: Finding qualified workers to fix household problems/necesities and lost and found items in the university campus.

## Interview Structure

### Household chores

<em>Introduction: </em> 
<ol>
     <li>Briefly, present yourself to the interviewee.</li>
     <li>State the purpose of the interview</li>
     <li>Ask him/her permissions to record</li>
</ol>

<em>Questions: </em>

<ul>
     <li>When you require a mantainance or reparation service in your household, who do you normally contact?</li>
     <li>How were your previous experiences hiring a worker to fix household related damages? Can you tell us one of the experiences you've had?</li>
     <li>If you don't know of anyone that fixes a particular damage you have, what mediums or channels do you use to find a worker for the job?</li>
     <li>When you hire a worker to fix or mantain something in your household, how do you make sure they are qualified for the job?</li>
     <li>Did you ever take a long time finding the right worker to fix a household problem? (Yes/No) ¿How did it felt that the process was so long?</li>
     <li>If you had to grade the process of hiring a worker in a scale from 1 to 5, where 1 is hard and tedious and 5 is easy and fast?</li>
     <li>Which method of payment would you like to use to pay this kind of services?</li>
</ul>

### Lost and found items

<em>Introduction: </em> 
<ol>
     <li>Briefly, present yourself to the interviewee.</li>
     <li>State the purpose of the interview</li>
     <li>Ask him/her permissions to record</li>
</ol

<em>Questions: </em>
<ul>
<li>Have you ever lost an object inside the university campus?</li>
<li>When you lose objects in the university, what steps do you take to find them?</li>
<li>How hard or easy is the process of finding a lost object in the university?</li>
<li>When you find a lost object (that is not yours) inside the campus what do you do?</li>
<li>How hard or easy is the process of giving back a lost object that you found to it’s owner or lost and found booth?</li>
<li>Could you narrate an experience where you used the lost and found social network groups or the univerity L&F booth to search a lost object?</li>
<li>How long did it take you to find out about the groups in Facebook for lost and found objects in the university or about the lost and found booth?</li>
<li>If you had to grade the process of searching for a lost object in the university in a scale from 1 to 5, where 1 is hard and tedious and 5 is easy and fast?</li>
</ul>

## Interviews Summary
### Drive folder with all the interviews and highlights for both problems:
-   https://drive.google.com/drive/folders/1Ii1wLc9UU74XDsYzPpobKzvx5f3eKRPF?usp=sharing

## Situations



### Household issues

## Interview 1

| Situation | What? | How? | Why? | Who? |
| ------ | ------ | ------ | ------ | ------ |
| 1 | Sometimes the services that she receives in her home don’t have the expected results. | It looks like an inconvenient that turns her up upset. | She had problems with someone that she contracted to repair a cooking pot at her house. | A young adult woman that lives with his parents. |
| 2 | She is dissatisfied with services that she received previously in her home. | She thinks that, in general, she wasn’t having very good experiences. | Because her experiences were mediocre | A young adult woman that lives with his parents. |
| 3 | The terms of the services are unclear, resulting in disagreements. | She had a bad experience in which one person acted in a dishonest way (according to her) | Because the terms that were unclear for both parts. | A young adult woman that lives with his parents. |
| 4 | Troubles contacting people that provide housing services. | She experienced a situation in which it was difficult to contact the person that was providing certain service. | Because this person wasn't taking her calls and his workplace was closed. Besides, he doesn't use other communication media. | A young adult woman that lives with his parents. |
| 5 | Troubles finding people that can help with household issues. | It's hard to find people that can help with certain issues (for example, to fix an iron). | People that can help are split in different neighborhood and sometimes it isn’t easy to find them. | A young adult woman that lives with his parents. |
| 6 | Lack of a way to prove that the person providing the service is qualified. | There is no way to be sure that the person that offers a service is qualified to do that. | Generally, the process is made informally, through verbal recommendations. | A young adult woman that lives with his parents. |
| 7 | Lack of offering related with the services that are needed in the home. | There are not a lot of options when a service in the house is needed. | To find a service from a person, normally the options are people that are near, or people recommended through voice-to-voice. | A young adult woman that lives with his parents. |
| 8 | The process of finding a person that can assist in home issues is delayed. | It takes time to find someone that can help with the issue.  | Because not always these people are nearby. | A young adult woman that lives with his parents. |
| 9 | During the pandemic, is more complex to find people that can assist with issues related with the home. | There are not a lot of people that can help with household issues (maintaining, reparations) | Because of the pandemic, there are fewer people offering this kind of services in the neighborhood. | A young adult woman that lives with his parents. |
| 10 | Lack of confidence in Internet options for this kind of services | Normally, people prefer known people for this kind of work.  | Because of the culture of the people, they tend to put confidence in known people. | A young adult woman that lives with his parents. |

## Interview 2

| Situation | What? | How? | Why? | Who? |
| ------ | ------ | ------ | ------ | ------ |
| 1 | Ask to the “todero” of the building fix any element of the house.   | Calling reception to make an appointment.   | She trusts in the building workers and likes their quality.  | A young adult that lives alone.  | cell |
| 2 | Looking for a recommended person to change the locks of the apartment.  | Asking in reception if they know an external person to fix the locks.  | The workers of the building don’t know how to fix this.  | A young adult that lives alone.  | cell |
| 3 | Looking for someone else to fix something from the apartment.  | Browsing in Facebook Market  | The workers of the building aren’t available.  | A young adult that lives alone.  | cell |
| 4 | Checking the quality of the services of the building “todero”  | Give a simple view.   | She trusts in the building workers and likes their quality.  | A young adult that lives alone.  | cell |
| 5 | Checking the quality of the services of the person found in Facebook Market.  | Checking if the person really fixed the problem.  | Is a strange person and she doesn’t know the quality of his services.  | A young adult that lives alone.  | cell |
| 6 | Find someone to install TV.  | Browsing in Google.  | It was her first day in the building and didn’t know the services of the building workers.  | A young adult that lives alone.  | cell |
| 7 | Evaluating the level of confidence  | Analyzing the user information and photos.  | Anyone can offer their services in Facebook Market and she needs quality.  | A young adult that lives alone.  | cell |
| 8 | Find a person to fix elements when she lived in Villavicencio.  | Browsing in Google.  | She was living alone in a building but without workers to fix stuff.   | A young adult that lives alone.  | cell |
| 9 | Pay to the person with Nequi.  | Using Bancolombia app  | It’s easy to use and she doesn’t like to use cash.  | A young adult that lives alone.  | cell |
| 10 | Pay to the person with credit card.  | Using PayPal platform.  | She wants to avoid any contact with the workers due to the pandemic.  | A young adult that lives alone.  | cell |


## Interview 3

| Situation | What? | How? | Why? | Who? |
| ------ | ------ | ------ | ------ | ------ |
| 1 | Fixing a damaged appliance.  | Calling the company, of the damaged appliance, customer service.  | She trusts the company would send a coalified professional.  | An adult that lives with a 5-person family.  | cell |
| 2 | Buying a household insurance.  | Hire an insurance carrier.  | She trusts the company would solve all household problems.  | An adult that lives with a 5-person family.  | cell |
| 3 | Find an expert to make a maintenance.  | Contacting the company of the appliance.  | She trusts the company would send a coalified professional.  | An adult that lives with a 5-person family.  | cell |
| 4 | Find an expert to make a repair.  | Search via internet in different webpages for an expert.  | She is anxious because the situation is an emergency.  | An adult that lives with a 5-person family.  | cell |
| 5 | Evaluating the confidence of the expert.  | See where he works at.  | If he is linked with a good company is more probably be a trustworthy worker for her.  | An adult that lives with a 5-person family.  | cell |
| 6 | Evaluating the results of the service.  | Asking the expert, the status of the object inspected.  | She trusts the expert observation because of her inexperience with the topics.  | An adult that lives with a 5-person family.  | cell |
| 7 | Fixing a damaged appliance.  | Asking for the insurance company to cover the situation.  | She trusts the company she pays will hire an expert to solve the situation.  | An adult that lives with a 5-person family.  | cell |
| 8 | Be discontent with a service made.  | The expert makes the situation worst.  | She is mistrustful because of a company who hired that expert.  | An adult that lives with a 5-person family.  | cell |
| 9 | Pay the service with a virtual wallet.  | Using an online application.  | In case of an emergency, she needs to pay in an easy and fast way.  | An adult that lives with a 5-person family.  | cell |
| 10 | Pay the service with a credit card.  | Using a bank account.  | In case of an emergency, she needs to pay in an easy and fast way.  | An adult that lives with a 5-person family.  | cell |


### Lost and found items

## Interview 1

| Situation | What? | How? | Why? | Who? |
| ------ | ------ | ------ | ------ | ------ |
| 1 | The user finds and object inside of the university |  He is living a classroom and sees that someone left an umbrella on the floor.  | Someone forgot the umbrella on the floor.   | Middle age man, is studying in the university and has to go to another classroom.  | cell |
| 2 |  The users is thinking what to do with the found object.  | He is considering to whom he can give the object he found.    |  He is thinking about the person who lost the item and want to give it back.  |  Middle age man, is studying in the university He is a new student.  | cell |
| 3 | The user talks to another student to know which way we can give the item back. | He searches for an older student or someone else you might have lost or found something. | The other student probably knows to whom the user can give the found object.  | Middle age man, it’s his first time founding a lost item.  | cell |
| 4 | The user wants to enter the Facebook group for lost items. | The user wants to enter the Facebook group for lost items. | He wants to report the found object. | Middle age man, who just found a lost item and uses Facebook.   | cell |
| 5 |  The user reports the found object in the Facebook group.  | He goes to the Facebook group and make a post in the feed. |  He wants to report the found object.  | Middle age man, who just found a lost item and uses Facebook. | cell |
| 6 | Give the item to the office of found items.  | He wants to give the found item to his owner but doesn’t want to search for the owner.   |  He wants to give the item back but doesn’t has the time to search for the owner.  | Middle age man whom has to go home because he has an exam due next day.   | cell |
| 7 | The user realized he lost something. | He had 10 minutes to arrive to another classroom and when we arrived, he realized that something is missing. | Because of the rush he had to leave the classroom running. |  Middle age man, who has to classes and no time between them.  | cell |
| 8 |  The user searches what he can do to find the item.  |  He asks a friend what he can do to find the lost item.  | He wants to get the lost item back. | Middle age man, is the first time he lost something. | cell |
| 9 | The user searches the lost item in the Facebook group.  | He goes to Facebook, enters the group and searches on the feed of the last few days. | He wants to retrieve the item and knows that maybe someone found it. | Middle age man, who just lost something. | cell |
| 10 | Search the lost item in the found object office.   | He goes to the office and asks for the lost item. | He wants to retrieve the lost item.   | Middle age man whom just lost something and didn’t found it in the Facebook group.  | cell |

## Interview 2

| Situation | What? | How? | Why? | Who? |
| ------ | ------ | ------ | ------ | ------ |
| 1 | The student must do a line to recover his lost object | Often the line is long, and he can’t do it because of time restrictions in his timetable. | The break period in which he goes to the lost and found booth matches with a lot of other students causing congestion | A university student that is trying to recuperate a los object. | cell |
| 2 | A student encounters an object that seems lost but isn’t sure about taking it to L&F booth because of the only location. | He has a transport (E.g., Wheels) that leaves at a certain hour, or he wants to arrive to public transport before congestion hours | Even though the location of the L&F booth is “Central” in the university campus, it may be far away from where a person needs to take his transport. | A student that encounters a lost object | cell |
| 3 |  A person lost something valuable and is very concerned about the chances of finding their lost object. |  He/She is desperate because is a valuable object (E.g., laptop, phone, wallet) |  She/He is very desperate because there is a chance that the person who finds it keeps it. | She/He is very desperate because there is a chance that the person who finds it keeps it. |  A person that lost a valuable object inside the campus. |
| 4 |  A person who finds an object doesn’t want to get in the process of giving it in the L&F booth because it can be a long and time-consuming process. |  He is considering if the process is going to take him a lot of time, which he doesn’t have at the moment |  He is in a change of class and must go between buildings far away in the campus. | A community member. | cell |
| 5 |  A person is tempted to fake an object he lost. | If a person needs a charger or some headphones, he may be tempted to say he lost one in order to get one for free in the L&F booth. | He has economic problems and can’t afford to buy new ones. | A member of the university who has access to the L&F  booth. | cell |
| 6 |  For a first-year student it may not be clear what is the process of finding a lost object |  He is embarrassed because he doesn’t know where he can ask for his lost object or doesn’t now the location of the L&F booth. |  He is embarrassed to ask because older students may prank him or give him an imaginary location. |  A first-year student. | cell |
| 7 | A student loses a food container and food containers can’t be accepted in L&F booth, so they throw them away | He is sad because if he doesn’t find his Tupperware the same day, he lost it they will throw them away. | Tupperware can be expensive, or they can have a sentimental attachment to it so losing it is sad. |  Many people who take their lunches to the university do it because they don’t have a lot of resources. It can be a low resource student. | cell |
| 8 |  A student uses social media to find the owner of a lost object he encountered | He is hopeful he can find the owner of the object by posting in social media. | He is doing it because he is empathetic about the person who lost an object | A student who belongs to a L&F group from the university. | cell |
| 9 | A student uses social media to find an object he lost | He is hopeful he can find the object he lost  | He does not want to lose a personal belonging, furthermore if the object is valuable.  | A student who belongs to a L&F group form the university | cell |
| 10 | Contacting a person after finding the post of someone with their lost object | Happy he found the lost object  |  He is relieved he didn’t lose something valuable for him |  A student who found his lost item. | cell |

## Interview 3

| Situation | What? | How? | Why? | Who? |
| ------ | ------ | ------ | ------ | ------ |
| 1 | Not all people are aware of the existence of the lost and found room  | The student did not know the room and had to resort to an app to find his phone  | Information on the existence of the lost and found room is unclear  | People who lose a property for the first time  | cell |
| 2 | You should go to the lost and found office to know if the lost property is there.  | The user does not know if the missing object is in the room until he goes to the room to check  | There is no other information system to consult  | People who lose a property  | cell |
| 3 | The user only knows that someone has found his object until he goes and asks for it in the lost and found room  | It is not possible to know if someone else found the object and is waiting to take it to the lost and found room  | There is no other information system to consult  | People who lose a property  | cell |
| 4 | Not all people know the location of the lost and found room  | The student did not know the location of the room the first time he lost a property  | Information on the location of the lost and found room is not clear  | People who lose a property for the first time  | cell |
| 5 | There is no digital inventory control  | When claiming an object, it is evident that the inventory management is paper-based.  | Inventory management is done in a ledger.  | Person in charge of the room   | cell |
| 6 | Lost and found room opening hours are not clear.  | The first time the user went to the lost and found room, it was closed  | Because the interviewee went at a time when the room was closed  | People who lose a property  | cell |
| 7 | You can only leave an object found at specific times  | When a product is found, it must be delivered within the allotted hours  | The person in charge of the office is not available all the time  | People who find an object  | cell |
| 8 | Lost items in some cases are not immediately delivered to the lost and found room  | Users cannot know if someone found their lost object  | There is no way to record that a lost object was found.  | People who lose a property  | cell |
| 9 | Unclaimed items are donated at the end of the quarter  | The user does not know until which dates he can claim his lost object  | There is no information system to consult the dates on which objects will be donated  | People who lose a property  | cell |
| 10 | A user may carry an object that is not accepted by the lost and found room.  | It is not clear which items can be taken to the lost and found room and which cannot  | There is no register of what items can be taken to the lost and found room  | People who find an object  | cell |

## Journey maps

### Household

#### Process of finding a worker to fix a household problem
![Lost and found Journey map](https://cdn.discordapp.com/attachments/775911066730627076/807490077412032532/unknown.png)


### Lost and found

#### Process of finding an object

![Lost and found Journey map](https://cdn.discordapp.com/attachments/775911066730627076/807489645017170000/unknown.png)

## Brainstorming

[Brainstorming mural](https://app.mural.co/t/objetosperdidos1902/m/objetosperdidos1902/1612586220667/521f06ebed94fa8e3ff02fb54c8597a273ee4da8)

<p>For each problem, we divided the group into two subgroups. Each of these groups had to purpose ideas (with yellow stickers) about the problem and, by the blue stickers, we added possible features to that 'general feature'. After that we switched between the subgroups in order to enrich the ideas that each integrant contributed.</p>

## Prototype Video

[Demonstration Video](https://drive.google.com/file/d/1EvS9hnLc-1TQSUrmidy738v5vW-sWLVZ/view?usp=sharing)




